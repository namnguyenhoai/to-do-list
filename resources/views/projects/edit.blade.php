<!-- /resources/views/projects/edit.blade.php -->
@extends('layouts.app')
 
@section('content')
    <h2>Edit Project</h2>
 
    {!! Form::model($project, ['method' => 'PATCH', 'route' => ['projects.update', $project->slug]]) !!}
        @include('projects/partials/_form', ['submit_text' => 'Edit Project']) // goi partial form, sau do truyen tham so cho ten submit button
    {!! Form::close() !!}
@endsection